---
title: "My Journey Into Linux"
date: 2023-08-30T19:30:31+04:00
ShowReadingTime: true
ShowPostNavLinks: true
hideFooter: true
author: "Me"
disableAnchoredHeadings: true
tags: ['linux', 'raspberry pi', 'opensource', 'personal']
---

# My Linux journey

It all started back in my university days when NPCC approached the University of Mauritius with an IoT project to help SMEs in their operations. My friend [Pranav](https://www.linkedin.com/in/pranav-dhondea-71b374230) and I worked with Marine Biotechnology Products company. The company uses large containers to transport remains from a nearby Tuna Processing factory. Oil, dog meal were manufactured from those remains. It was difficult to keep track of those containers, whether they were clean up and ready for a new batch, whether they still contained raw materials and so on. Our aim was to provide a dashboard with details about each containers, such as where are they located at a specific time, what are their statuses.

## Devices

We were each given a Raspberry Pi model 3B+ as the micro-controller and its starter kit as well as a breadboard and jumper cables. The starter kit included a 32GB USB 3.0 Pendrive, an official Raspberry Pi power adapter, a 16GB SD card and a casing to protect the board.

## Exploring the RPi world

At that time, I had no knowledge about the Raspberry Pi and did not know how to control it or even how to install an operating system on the SD card. Following tutorials online, working days and night, I have been able to see the desktop environment of Raspbian.

> The very first command I ran on the Raspberry Pi was `sudo apt update`.

[Yash](https://www.linkedin.com/in/yashpaupiah), another university friend, informed me to give the RPi a static IP and connect it to my home's router via an ethernet cable. This will simulate a server. All controls that are done on the desktop environment can be done on the terminal.

I did it. There you go, my first `ssh` connection made to a server.

## LEMP stack

The most common tutorial when beginning to learn Linux is LEMP installation. I followed an online tutorial which guided me how to install nginx, mysql-server and php-fpm. My first website was up in an hour or so.

The real experience with Linux started when things started to go wrong on the RPi. I had the possibility to reflash the SD card or try to repair the system, at first I chose the easier one, but once I started getting at ease with Linux, I started fixing my bugs by doing R&D.

To summarize, my Linux journey really started with a Raspberry Pi. The next milestone was successfully installing Ubuntu Xenial on a Dell 7559 (with nVidia graphic chip on-board). After many trials, the installation was a success in the InstallFest of 2017.
