---
title: "About Me"
robotsNoIndex: true
---

**Namasté | Bonzour | Hello**

I'm Shravan Sudhveer Dwarka, a proud Mauritian hailing from the enchanting island of Mauritius. With a fervent passion for technology and an unquenchable curiosity, I've dedicated the past four years to honing my skills as a Level 2 Linux System Engineer at [Ocean DBA Ltd](https://www.oceandba.com).

My journey in the world of technology began at the prestigious University of Mauritius, where I graduated with a degree in Computer Science in 2019. Since then, I've been delving into the intricacies of Linux systems, constantly seeking ways to streamline and optimize processes. The ever-evolving nature of this field keeps me on my toes, and I find great satisfaction in overcoming challenges and contributing to the seamless functioning of complex systems.

Beyond the realm of terminals and commands, you'll find me indulging in a diverse array of interests. There's nothing quite like the thrill of casting a line into the deep blue ocean, indulging my love for fishing whenever I can. On weekends, I'm often found amidst the revving engines and vibrant energy of local car meets and rallies, where the passion for automobiles runs deep.

But it's not all about technology and hobbies. I firmly believe in giving back to the community that has nurtured me. As a volunteer for [Isha Mauritius](https://www.facebook.com/ishamauritius) Yoga programs, I've had the privilege of contributing to the well-being and mindfulness of others. This experience has enriched my life in countless ways, reminding me of the importance of balance and inner harmony.

This website serves as a platform to share my journey, experiences, and insights. From tech tips and Linux know-how to tales of fishing escapades and the camaraderie of car gatherings, I invite you to join me in exploring the multifaceted tapestry of my life.

Thank you for being a part of my story.

Warmly,  <br>  
Shravan
